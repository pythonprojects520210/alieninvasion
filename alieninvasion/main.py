import pygame
from spaceship import Spaceship
from alien_spaceship import AlienSpaceship
from text import Text
from pathlib import Path
from sys import exit

class Game:
    def __init__(self, resolution: tuple, title_font_size: int, text_font_size: int, score_font_size: int, framerate) -> None:
        """Creating main class for management the game"""
        assert len(resolution) == 2, f'Resoulition has exactly 2 values'
        assert title_font_size > 10, f'Title font size min 10 px'
        assert text_font_size > 10, f'Text font size min 10 px'

        # initialization of the gamme
        pygame.init()

        # game management objects
        self.framerate = framerate
        self.__resolution = resolution             # resolution of the game window
        self.__center_x = (self.__resolution[0]/2) # centrum position in the x-plane
        self.missiles = []                         # all missiles list
        self.alien_spaceships = []                 # all alien list

        self.__title_font_size = title_font_size
        self.__text_font_size = text_font_size
        self.__score_font_size = score_font_size
        self.__paths = {
            'pygame_icon': Path.cwd() / 'assets' / 'images' / 'PNG' / 'effect_purple.png',
            'pygame_background_image': Path.cwd() / 'assets' / 'images' / 'PNG' / 'bgi_1024_768.png',
            'pygame_MadimiOne_font': Path.cwd() / 'assets' / 'fonts' / 'MadimiOne-Regular.ttf',
            'pygame_kenvector_future_font': Path.cwd() / 'assets' / 'fonts' / 'kenvector_future.ttf',
            'pygame_spaceship_player_img': Path.cwd() / 'assets' / 'images' / 'PNG' / 'ship_A.png',
            'pygame_spaceship_alien_img': Path.cwd() / 'assets' / 'images' / 'PNG' / 'ship_sidesA.png',
            'pygame_spaceship_player_flame_img': Path.cwd() / 'assets' / 'images' / 'PNG' / 'effect_yellow.png',
            'pygame_spaceship_missile': Path.cwd() / 'assets' / 'images' / 'PNG' / 'yellow_tick.png',
        }
        self.colors = {
            'main_color': (42, 219, 36),
            'second_color': (213, 36, 219),
            'second_color_hoover': (151, 23, 156),
            'button_bgc': (117, 117, 117),
            'button_bgc_margins': (84, 84, 84),
        }
        
        # initialization of game and window objects
        self.__initialization()
         

    def __initialization(self):
        """Starting pygame and setting basic styles"""

        # setting of game icon
        self.__pygame_icon_img = pygame.image.load(self.__paths['pygame_icon'])
        pygame.display.set_icon(self.__pygame_icon_img)

        # setting title of the display
        pygame.display.set_caption('Alien Game')

        # pygame objects 
        self.__game_started = False
        self.__screen = pygame.display.set_mode(self.__resolution)
        self.__clock = pygame.time.Clock()
        self.__title_font = pygame.font.Font(self.__paths['pygame_MadimiOne_font'], self.__title_font_size)
        self.__text_font = pygame.font.Font(self.__paths['pygame_kenvector_future_font'], self.__text_font_size)
        self.__score_font = pygame.font.Font(self.__paths['pygame_kenvector_future_font'], self.__score_font_size)

        # player objects
        self.__player_spaceship = Spaceship(self.__paths['pygame_spaceship_player_img'], self.__center_x, self.__resolution[1], self.__paths['pygame_spaceship_player_flame_img'], self.__paths['pygame_spaceship_missile'])
        self.__player_points = 0

        # texts
        self.title = Text(self.__title_font, 'Alien Invasion', self.colors['main_color'], (self.__center_x, 100))
        self.start_text = Text(self.__text_font, 'Start', self.colors['second_color'], (self.__center_x, 300))
        # self.option_text = Text(self.__text_font, 'Option', self.colors['second_color'], (self.__center_x, 360))
        self.exit_text = Text(self.__text_font, 'Exit', self.colors['second_color'], (self.__center_x, 380))
        self.points_text = Text(self.__score_font, f'Points: {self.__player_points}', self.colors['second_color'], (self.__resolution[0]-(self.__score_font_size*4.5), (self.__score_font_size*2)))

        # pygame timers - for creating aliens
        self.timer_event = pygame.event.custom_type()
        pygame.time.set_timer(self.timer_event, 2000)

        # pygame sufraces
        self.__background_surface = {
            'image': pygame.image.load(self.__paths['pygame_background_image']).convert(),
            'pos_x': 0,
            'pos_y': 0,
            'pos_y_second': -abs(self.__resolution[1])
            }
        
    def __background_loop(self):
        """Loop the background image"""

        self.__background_surface['pos_y'] += 1
        self.__background_surface['pos_y_second'] += 1
        
        if self.__background_surface['pos_y_second'] == 0:
            self.__background_surface['pos_y'] = -768
        if self.__background_surface['pos_y_second'] == 768:
            self.__background_surface['pos_y_second'] = -768

        self.__screen.blit(self.__background_surface['image'], (self.__background_surface['pos_x'], self.__background_surface['pos_y']))
        self.__screen.blit(self.__background_surface['image'], (self.__background_surface['pos_x'], self.__background_surface['pos_y_second']))
            
    def __menu_loop(self):
        """Action in menu"""
        # adding game title at starting program 
        self.__screen.blit(self.title.text, self.title.rect)

        self.__screen.blit(self.start_text.text, self.start_text.rect)
        # self.__screen.blit(self.option_text.text, self.option_text.rect)
        self.__screen.blit(self.exit_text.text, self.exit_text.rect)

    def __creating_alien_spaceship(self):
        alien_spaceship = AlienSpaceship(self.__paths['pygame_spaceship_alien_img'], -64, 3)
        self.alien_spaceships.append(alien_spaceship)

    def __exit_game(self):
        """Closing the game"""
        pygame.quit()
        exit()

    def __reset_game(self):
        self.__game_started = False
        self.__player_spaceship = Spaceship(self.__paths['pygame_spaceship_player_img'], self.__center_x, self.__resolution[1], self.__paths['pygame_spaceship_player_flame_img'], self.__paths['pygame_spaceship_missile'])
        self.__player_points = 0
        self.points_text = Text(self.__score_font, f'Points: {self.__player_points}', self.colors['second_color'], (self.__resolution[0]-(self.__score_font_size*4.5), (self.__score_font_size*2)))


        self.alien_spaceships = []
        self.missiles = []

        self.__background_loop()
        self.__start_game()

    def __update_points(self):
        self.__player_points += 1
        self.points_text = Text(self.__score_font, f'Points: {self.__player_points}', self.colors['second_color'], (self.__resolution[0]-(self.__score_font_size*4.5), (self.__score_font_size*2)))

    def __start_game(self):
        """Actions after clicking Start option by player"""

        # handling loosing the game
        if self.__player_spaceship.lifes == 0:
            self.__reset_game()

        # adding player spaceship
        self.__screen.blit(self.__player_spaceship.player_image, self.__player_spaceship.player_rect)
        self.__screen.blit(self.__player_spaceship.flame_image, self.__player_spaceship.flame_rect)
        self.__screen.blit(self.points_text.text, self.points_text.rect)

        # handling missiles shooting
        if self.missiles:
            for missile in self.missiles:
                if missile.rect.top > -32:
                    missile.missile_trajectory()
                    self.__screen.blit(missile.img, missile.rect)
                else:
                    # removing missile after reaching the top
                    self.missiles.remove(missile)
                    del missile

        # handling aliens spaceships
        if self.alien_spaceships:
            for alien in self.alien_spaceships:
                if alien.lifes:
                    if alien.rect.top > self.__resolution[1]:                    
                        self.__player_spaceship.lifes -= 1

                        # removing alien spaceship
                        self.alien_spaceships.remove(alien)
                        del alien
                    else:
                        alien.ships_flight()
                        self.__screen.blit(alien.image, alien.rect)
                else:
                    self.alien_spaceships.remove(alien)
                    del alien
                    self.__update_points()

        for missile in self.missiles:
            for alien in self.alien_spaceships:
                if missile.rect.colliderect(alien.rect):
                    self.missiles.remove(missile)
                    alien.lifes -= 1
            del missile

    def start_game_loop(self):
        """Starting game loop and setting the surface"""

        while True:
            mouse_pos = pygame.mouse.get_pos()

            for event in pygame.event.get():
                # handling quiting the game
                if event.type == pygame.QUIT:
                    self.__exit_game()

                # mouse efect in menu loop
                elif not self.__game_started:
                    if self.start_text.rect.collidepoint(mouse_pos):
                        self.start_text.change_text_color(self.colors['second_color_hoover'])

                        if pygame.mouse.get_pressed()[0]:
                            self.__game_started = True
                    elif not self.start_text.rect.collidepoint(mouse_pos):
                        self.start_text.change_text_color(self.colors['second_color'])

                    # if self.option_text.rect.collidepoint(mouse_pos):
                    #     self.option_text.change_text_color(self.colors['second_color_hoover'])

                    #     if pygame.mouse.get_pressed()[0]:
                    #         print('Option')
                    # elif not self.option_text.rect.collidepoint(mouse_pos):
                        # self.option_text.change_text_color(self.colors['second_color'])

                    if self.exit_text.rect.collidepoint(mouse_pos):
                        self.exit_text.change_text_color(self.colors['second_color_hoover'])

                        if pygame.mouse.get_pressed()[0]:
                            self.__exit_game()
                    elif not self.exit_text.rect.collidepoint(mouse_pos):
                        self.exit_text.change_text_color(self.colors['second_color'])

                # handling player shooting action
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        self.missiles.append(self.__player_spaceship.get_missile())

                # handling timer action
                elif event.type == self.timer_event:
                    self.__creating_alien_spaceship()

            # handling player ship control
            keys = pygame.key.get_pressed()
            if keys[pygame.K_LEFT]:
                self.__player_spaceship.move_ship(pygame.K_LEFT)
            if keys[pygame.K_RIGHT]:
                self.__player_spaceship.move_ship(pygame.K_RIGHT, self.__resolution[0])
            
            
            # inifinite background loop 
            self.__background_loop()

            # start screen since start the game
            if not self.__game_started:
                self.__menu_loop()
            else:
                self.__start_game()
            

            # framerate refresh
            pygame.display.flip()
            self.__clock.tick(self.framerate)
            


if __name__ == '__main__':
    game = Game(resolution = (1024, 628), title_font_size = 60, text_font_size = 40, score_font_size = 20, framerate = 60)
    game.start_game_loop()