import pygame
from pathlib import Path
from missile import Missile

class Spaceship:
    def __init__(self, img_path: Path, x: int, y: int, flame_img_path: Path = None, missile_img_path: Path = None) -> None:
        """Creating spaceship object"""
        # attributes
        self.flame_img_path = flame_img_path
        self.missile_img_path = missile_img_path
        self.x = x
        self.y = y
        self.lifes = 2

        # ship objects
        self.player_image = pygame.image.load(img_path).convert()
        self.player_rect = self.player_image.get_rect(midbottom = (self.x, self.y))

        # flame objects
        self.flame_image = pygame.image.load(self.flame_img_path).convert_alpha()
        self.flame_image = pygame.transform.scale(self.flame_image, (38,38))
        self.flame_rect = self.flame_image.get_rect(midbottom = (self.x, self.y))

        self.player_rect.top -= self.player_rect.height/2.2

    def move_ship(self, direction, max_right=None) -> None:
        """Moving player spaceship by arrows"""

        if direction == pygame.K_LEFT:
            if self.player_rect.left >= 0:
                self.player_rect.left -= 5
                self.flame_rect.left -= 5
        elif direction == pygame.K_RIGHT:
            if self.player_rect.left <= max_right - self.player_rect.width:
                self.player_rect.left += 5
                self.flame_rect.left += 5

    def get_missile(self) -> Missile:
        """Creating missile object"""
        missile = Missile(self.missile_img_path, (self.player_rect.left+self.player_rect.width/2), self.y)
        return missile
        
