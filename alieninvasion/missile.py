import pygame
from pathlib import Path

class Missile:
    def __init__(self, missile_img_path: Path, x: int, y: int) -> None:
        """Creating missile object"""
        self.missile_img_path = missile_img_path
        self.x = x
        self.y = y

        self.img = pygame.image.load(self.missile_img_path)
        self.rect = self.img.get_rect(midbottom = (self.x, self.y))

        self.rect.top -= 80

    def missile_trajectory(self):
        """Upward movement of the projectile"""
        self.rect.top -= 3