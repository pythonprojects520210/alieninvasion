import pygame

class Text:
    def __init__(self, font: pygame.font.Font, text: str, color: str, pos: tuple) -> None:
        self.font = font
        self.text_value = text
        self.color = color
        self.pos = pos

        self.text = self.font.render(self.text_value, True, self.color)
        self.rect = self.text.get_rect(center=self.pos)
    
    def change_text_color(self, new_color:str) -> None:
        self.text = self.font.render(self.text_value, True, new_color)