import pygame
import random
from pathlib import Path

class AlienSpaceship:
    def __init__(self, img_path: Path, y: int, speed: int, lifes: int = 2) -> None:
        """Creating alien spaceship object"""
        self.img_path = img_path 
        self.x = random.randint(32, 596)
        self.y = y

        self.speed = speed
        self.lifes = lifes

        # alien ship object
        self.image = pygame.image.load(self.img_path).convert_alpha()
        self.image = pygame.transform.rotate(self.image, 180)
        self.rect = self.image.get_rect(midtop = (self.x, self.y))

    def ships_flight(self):
        self.rect.top += self.speed