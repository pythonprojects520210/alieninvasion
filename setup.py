from setuptools import setup, find_packages

setup(
    name="alien_game",
    version='0.0.10',
    author="Mikołaj Pilarczyk",
    author_email="mikolaj.kontakt3200@gmail.com",
    description="Simple 2d game where you have to defeat aliens in space",
    url="https://gitlab.com/pythonprojects520210/aliengame/alieninvasion.git",
    long_description_content_type="text/markdown",
    long_description="2d game where you fly your ship and shoot to the alien ships",
    packages=find_packages(),
    install_requires=['opencv-python', 'pyautogui', 'pyaudio'],
    keywords=['game', 'space', 'aliens', 'shoot'],
)